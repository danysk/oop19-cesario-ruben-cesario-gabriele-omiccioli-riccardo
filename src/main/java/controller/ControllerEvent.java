package controller;

/**
 * Models an event happened in the controller.
 */
public enum ControllerEvent {

        /**
         * Shoot event.
         */
        SHOOT,

        /**
         * Projectile-explosion event.
         */
        PROJECTILE_EXPLOSION,

        /**
         * Explosion event.
         */
        EXPLOSION,

        /**
         * Next level event.
         */
        NEXT_LEVEL;
}
