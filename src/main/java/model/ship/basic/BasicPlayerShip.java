package model.ship.basic;

import model.ship.PlayerShip;

/**
 * Models a BasicSpaceShip belonging to a player, identified by a name, with
 * a certain score.
 */
public interface BasicPlayerShip extends BasicSpaceShip, PlayerShip { }
